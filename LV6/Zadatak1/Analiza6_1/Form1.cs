﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Analiza6_1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }


        private void Form1_Load(object sender, EventArgs e)
        {

            label5.Visible = false;
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {

            double o1, o2;
            if (comboBox1.SelectedItem == null) MessageBox.Show("Niste odabrali operaciju!", "Pogreška");
            else if (textBox1.Text == String.Empty) MessageBox.Show("Niste unijeli prvi operand!", "Pogreška");
            else if (textBox2.Text == String.Empty && textBox2.Visible == true) MessageBox.Show("Niste unijeli drugi operand!", "Pogreška");
            else if (!double.TryParse(textBox1.Text, out o1)) MessageBox.Show("Prvi operand nije broj!", "Pogreška");
            else if (!double.TryParse(textBox2.Text, out o2) && textBox2.Visible == true) MessageBox.Show("Drugi operand nije broj!", "Pogreška");
            else
            {
                if (comboBox1.SelectedItem.ToString() == "+")
                {
                    label5.Text = textBox1.Text + " + " + textBox2.Text + " = " + (o1 + o2).ToString();
                    textBox1.Text = string.Empty;
                    textBox2.Text = string.Empty;
                    label5.Visible = true;
                }
                else if (comboBox1.SelectedItem.ToString() == "-")
                {
                    label5.Text = textBox1.Text + " - " + textBox2.Text + " = " + (o1 - o2).ToString();
                    textBox1.Text = string.Empty;
                    textBox2.Text = string.Empty;
                    label5.Visible = true;
                }
                else if (comboBox1.SelectedItem.ToString() == "/")
                {
                    if (o2 == 0) MessageBox.Show("Dijeljenje s nulom nije moguće!", "Pogreška");
                    else
                    {
                        label5.Text = textBox1.Text + " / " + textBox2.Text + " = " + (o1 / o2).ToString();
                        textBox1.Text = string.Empty;
                        textBox2.Text = string.Empty;
                        label5.Visible = true;
                    }
                }
                else if (comboBox1.SelectedItem.ToString() == "*")
                {
                    label5.Text = textBox1.Text + " * " + textBox2.Text + " = " + (o1 * o2).ToString();
                    textBox1.Text = string.Empty;
                    textBox2.Text = string.Empty;
                    label5.Visible = true;
                }
                else if (comboBox1.SelectedItem.ToString() == "sin")
                {
                    label5.Text = "sin(" + textBox1.Text + ") = " + Math.Sin(o1).ToString();
                    textBox1.Text = string.Empty;
                    textBox2.Text = string.Empty;
                    label5.Visible = true;
                }
                else if (comboBox1.SelectedItem.ToString() == "cos")
                {
                    label5.Text = "cos(" + textBox1.Text + ") = " + Math.Cos(o1).ToString();
                    textBox1.Text = string.Empty;
                    textBox2.Text = string.Empty;
                    label5.Visible = true;
                }
                else if (comboBox1.SelectedItem.ToString() == "x^2")
                {
                    label5.Text = textBox1.Text + "^2 = " + (o1 * o1).ToString();
                    textBox1.Text = string.Empty;
                    textBox2.Text = string.Empty;
                    label5.Visible = true;
                }
                else if (comboBox1.SelectedItem.ToString() == "√")
                {
                    if (o1 < 0) MessageBox.Show("Nije moguće računati drugi korijen negativnog broja!", "Pogreška");
                    else
                    {
                        label5.Text = "√" + textBox1.Text + " = " + Math.Sqrt(o1).ToString();
                        textBox1.Text = string.Empty;
                        textBox2.Text = string.Empty;
                        label5.Visible = true;
                    }
                }
                else if (comboBox1.SelectedItem.ToString() == "log")
                {
                    label5.Text = "log(" + textBox1.Text + ") = " + Math.Log10(o1).ToString();
                    textBox1.Text = string.Empty;
                    textBox2.Text = string.Empty;
                    label5.Visible = true;
                }
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (comboBox1.SelectedItem.ToString())
            {
                case "+":
                case "-":
                case "/":
                case "*": textBox2.Visible = true; label2.Visible = true; break;
                default: textBox2.Visible = false; label2.Visible = false; break;
            }
        }
    }
}
