﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Analiza6_2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        List<string> sentences = new List<string>();
        Game g;

        public class Game
        {
            public int NumberOfLives { get; private set; }
            public string Word { get; private set; }
            public List<char> Attempts { get; set; }
            private string GuessedLetters = String.Empty;
            public int GuessedLettersCount { get; private set; }
            public string Label { get; private set; }
            public Game(string r)
            {
                Word = r;
                NumberOfLives = 5;
                GuessedLettersCount = 0;
                Attempts = new List<char>();
            }
            public bool IsFound(char c)
            {
                foreach (char d in GuessedLetters)
                {
                    if (d == c) return true;
                }
                return false;
            }
            public void Print()
            {
                Label = String.Empty;
                foreach (char c in Word)
                {
                    if (c == ' ') Label += c;
                    else if (IsFound(c)) Label += c;
                    else Label += '_';
                    Label += ' ';
                }
            }
            public void Guess(char c)
            {
                c = char.ToUpper(c);
                Attempts.Add(c);
                foreach (char d in Word)
                {
                    if (d == c)
                    {
                        GuessedLettersCount++;
                        GuessedLetters += c;
                    }
                }
                if (!IsFound(c))
                    NumberOfLives--;
            }
            public int spaceCount()
            {
                int space = 0;
                foreach (char c in Word)
                    if (c == ' ')
                        space++;
                return space;
            }
            public bool Tried(char c)
            {
                foreach (char d in Attempts)
                    if (c == d)
                        return true;
                return false;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {

            if (textBox1.Text == String.Empty) MessageBox.Show("You didnt enter a letter!", "Error");
            else if (g.Tried(char.ToUpper(textBox1.Text[0]))) { MessageBox.Show("You already guessed that letter!", "Error"); textBox1.Text = String.Empty; }
            else
            {
                g.Guess(textBox1.Text[0]);
                g.Print();
                label1.Text = g.Label;
                Lives.Text = g.NumberOfLives.ToString();
                textBox1.Text = String.Empty;
                if (g.NumberOfLives == 0)
                {
                    MessageBox.Show("You lost!", "Game over");
                    label1.Text = g.Word;
                    Letter.Visible = false;
                    textBox1.Visible = false;
                    button1.Visible = false;
                    Application.Exit();
                }
                else if (g.GuessedLettersCount + g.spaceCount() == g.Word.Length)
                {
                    MessageBox.Show("You Won!", "Congratulations!");
                    button1.Visible = false;
                    Letter.Visible = false;
                    textBox1.Visible = false;
                    Application.Exit();
                }
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

            using (System.IO.StreamReader reader = new System.IO.StreamReader("Sentences.txt"))
            {
                string line;
                line = reader.ReadLine();
                string[] parts = line.Split(',');
                foreach (string s in parts)
                    sentences.Add(s);
            }
            Random x = new Random();
            int n = x.Next(0, sentences.Count - 1);
            string clue = sentences[n];
            g = new Game(clue);
            g.Print();
            label1.Text = g.Label;
            Lives.Text = g.NumberOfLives.ToString();
        }
    }
}
